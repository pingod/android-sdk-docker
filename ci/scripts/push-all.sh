#!/usr/bin/env sh

filter=$1

images=$(docker image ls --filter "reference=$filter" --format "{{.Repository}}:{{.Tag}}")

for image in $images; do
    docker push "$image"
done
