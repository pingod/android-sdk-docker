#!/usr/bin/env bash

function main() {
    set -e
    set -o pipefail
    local config_dir=${XDG_CONFIG_HOME:-$HOME/.config}

    [[ -r /etc/asdk/config.conf ]] && source /etc/asdk/config.conf
    [[ -r $config_dir/asdk/config.conf ]] && source "$config_dir/asdk/config.conf"

    # ubuntu builds
    bash bin/builder -p bionic -T debian -b "BASE_IMAGE=buildpack-deps:bionic"
    bash bin/builder -p focal -T debian -a ubuntu -b "BASE_IMAGE=buildpack-deps:focal"
    bash bin/builder -p hirsute -T debian -b "BASE_IMAGE=buildpack-deps:hirsute"
    bash bin/builder -p xenial -T debian -b "BASE_IMAGE=buildpack-deps:xenial"
    bash bin/builder -p impish -T debian -b "BASE_IMAGE=buildpack-deps:impish"

    # debian builds
    bash bin/builder -p bullseye -T debian -a debian -b "BASE_IMAGE=buildpack-deps:buster" -b "JDK8_MIRROR=deb http://security.debian.org/debian-security stretch/updates main"
    bash bin/builder -p buster -T debian -b "BASE_IMAGE=buildpack-deps:buster" -b "JDK8_MIRROR=deb http://security.debian.org/debian-security stretch/updates main"
    bash bin/builder -p stretch -T debian -b "BASE_IMAGE=buildpack-deps:stretch"

    # node (debian-based) builds
    bash utils/build-node.sh -jb node:16 -p node -a node
    bash utils/build-node.sh -b node:15 -p node
    bash utils/build-node.sh -b node:14 -p node -a node-lts -a node-fermium
    bash utils/build-node.sh -b node:12 -p node -a node-erbium
    bash utils/build-node.sh -b node:10 -p node -a node-dubnium

    # alpine builds
    bash bin/builder -p alpine -T alpine -b "BASE_IMAGE=alpine:3"

    # node alpine builds
    bash utils/build-node.sh -T alpine -b node:16-alpine3.13 -p node -a node
    bash utils/build-node.sh -T alpine -b node:15-alpine3.13 -p node
    bash utils/build-node.sh -T alpine -b node:14-alpine3.13 -p node -a node-lts -a node-fermium
    bash utils/build-node.sh -T alpine -b node:12-alpine3.13 -p node -a node-erbium
    bash utils/build-node.sh -T alpine -b node:10-alpine3.13 -p node -a node-dubnium

    docker push "$IMAGE_NAME" --all-tags

    # asdk-builder -p node -a node-16 -T debian -b "BASE_IMAGE=node:16" -b "JDK8_MIRROR=deb http://security.debian.org/debian-security stretch/updates main"
}

main "$@"
