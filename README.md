# ReDemoNBR's Android SDK


## What is this image
This image contains required tooling for building Android applications.
It contains (at the time of this writing):
-   Base OS tooling (from base Debian, Ubuntu or Alpine images)
-   OpenJDK 11 and Gradle installation
-   Android Command Line Tools (version `8512546`) with:
    -   Build Tools (version `33.0.0`) _(not included in "cmdline" variants)_
    -   Platform Tools (version `33.0.2`) _(not included in "cmdline" variants)_
    -   Platform (version will depend on the [tag](#tags-explanation)). Also known as `API Level` (refer to [API Levels](#api-levels)) _(not included in "cmdline" and "tools" variants)_
    -   Build Tools, Command Line Tools and Platform Tools are also added to `PATH`

## Tags explanation
Format is: `<base_image>-<variant>` or `<base_image>-api-<level>`.

### Base images
Base image refer to the underlying image used to build this image on top of.
-   `alpine3.16` (alias: `alpine3`, `alpine`): refer to [`alpine`](https://hub.docker.com/_/alpine) image. Latest stable release from Alpine Linux
-   `alpine3.15`: refer to [`alpine`](https://hub.docker.com/_/alpine) image. Current stable release from Alpine Linux
-   `alpine3.14`: refer to [`alpine`](https://hub.docker.com/_/alpine) image. Previous stable release from Alpine Linux
-   `alpine3.13`: refer to [`alpine`](https://hub.docker.com/_/alpine) image. Old stable release from Alpine Linux
-   `bionic`: refer to [`buildpack_deps:bionic`](https://hub.docker.com/_/buildpack_deps) image. Old LTS release from Ubuntu Linux
-   `bullseye` (alias: `debian`): refer to [`buildpack_deps:bullseye`](https://hub.docker.com/_/buildpack_deps) image. Latest release from Debian Linux
-   `buster`: refer to [`buildpack_deps:buster`](https://hub.docker.com/_/buildpack_deps) image. Previous release from Debian Linux
-   `focal`: refer to [`buildpack_deps:focal`](https://hub.docker.com/_/buildpack_deps) image. Previous LTS release from Ubuntu Linux
-   `impish` (DEPRECATED): refer to [`buildpack_deps:impish`](https://hub.docker.com/_/buildpack_deps) image. Previous Rolling release from Ubuntu Linux
-   `jammy` (alias: `ubuntu`): refer to [`buildpack_deps:jammy`](https://hub.docker.com/_/buildpack_deps) image. Lastest LTS release from Ubuntu Linux
-   `node-18` (alias: `node`): refer to [`node:18`](https://hub.docker.com/_/node) image. Latest release from NodeJS, it uses `buildpack_deps:bullseye` as base
-   `node-18-alpine` (alias: `node-alpine`: refer to [`node:18-alpine`](https://hub.docker.com/_/node) image. Latest release from NodeJS, it uses `alpine:3.15` as base
-   `node-17`(DEPRECATED): refer to [`node:17`](https://hub.docker.com/_/node) image. Previous non-LTS release from NodeJS, it uses `buildpack_deps:bullseye` as base
-   `node-17-alpine` (DEPRECATED): refer to [`node:17-alpine`](https://hub.docker.com/_/node) image. Previous non-LTS release from NodeJS, it uses `alpine:3.15` as base
-   `node-16` (alias: `node-gallium`, `node-lts`): refer to [`node:16`](https://hub.docker.com/_/node) image. Current stable release from NodeJS, it uses `buildpack_deps:bullseye` as base
-   `node-16-alpine` (alias: `node-gallium-alpine`, `node-lts-alpine`): refer to [`node:16-alpine`](https://hub.docker.com/_/node) image. Current stable release from NodeJS, it uses `alpine:3.15` as base
-   `node-14` (alias: `node-fermium`): refer to [`node:14`](https://hub.docker.com/_/node) image. Latest LTS release from NodeJS, it uses `buildpack_deps:bullseye` as base
-   `node-14-alpine` (alias `node-fermium-alpine`): refer to [`node:14-alpine`](https://hub.docker.com/_/node) image. Latest LTS release from NodeJS, it uses `alpine:3.15` as base
-   `node-12` (alias: `node-erbium`): refer to [`node:12`](https://hub.docker.com/_/node) image. Previous LTS release from NodeJS, it uses `buildpack_deps:bullseye` as base
-   `node-12-alpine` (alias: `node-erbium-alpine`): refer to [`node:12-alpine`](https://hub.docker.com/_/node) image. Previous LTS release from NodeJS, it uses `alpine:3.15` as base

### Image Variants and API Levels
#### `redemonbr/android-sdk:<version>` | `redemonbr/android-sdk:<base-image>-<api-level>`
This is the defacto image, it bundles `platform-tools`, `build-tools` and Android Platform installed. The platform version installed depends on the API Level variant.
The API Level is also known as SDK version or simply "platform", this is the settings level that determine the compatibility with Android versions.
-   `api-33` (alias: `next`): The next level API available at the time of this writing that is available under _Developer Preview_ status, possibly corresponds to [**Android 13**](https://developer.android.com/about/versions/13). _Might be considered unstable_
-   `api-32` (alias: `latest`): The latest API level that mainly contains features for foldable devices on top of API 31, corresponds to [**Android 12L**](https://developer.android.com/about/versions/12/12L)
-   `api-31` (alias: `stable`): The stable API level available at the time of this writing, corresponds to [**Android 12**](https://developer.android.com/about/versions/12)
-   `api-30`: The minimum target API level allowed by Google Play at the time of this writing. Corresponds to [**Android 11**](https://developer.android.com/about/versions/11)
-   `api-29`: The minimum target API level allowed by Google Play until August 2021. **Still allowed for Wear OS applications**. Corresponds to [**Android 10**](https://developer.android.com/about/versions/10)
-   `api-28`: Old API level that is not supported by Google Play for Android applications at the time of this writing, **but still allowed for Wear OS applications**. Corresponds to [**Android 9 Pie**](https://developer.android.com/about/versions/pie)

For more information on SDK Level requirements in Google Play, please read [this](https://developer.android.com/google/play/requirements/target-sdk).

If `redemonbr/android-sdk:<version>` format is used (ex: `docker pull redemonbr/android-sdk:api-32`), the base image version is `jammy`
For more information about the tags, please check the list of available tags in Docker Hub

#### `redemonbr/android-sdk:<base-image>-tools` (tools)
This image is identical to `cmdline` variants, but also bundles `platform-tools` (v31.0.3) and `build-tools` (v32.0.0). This is a good image to be extended in case we are not supporting some of the API Levels here.
So you can create your Dockerfile with `FROM redemonbr/android-sdk:<version>-tools` and extend it.
Examplifying: the defacto images above are an extension of this one. Check our GitLab repositories to see how the Dockerfile extends this one.

#### `redemonbr/android-sdk:<base-image>-cmdline` (cmdline)
This image contains only the Android command line tools (aka `cmdline`) installed with the `sdkmanager` configured to be executed along with OpenJDK 8 and Gradle. It does not contain `platform-tools`, `build-tools` or any of the `platforms` installed for building applications.
This is a good image if you want to extend the base image installing different versions of `build-tools` or `platform`, or simply having a more robust tooling for building extremely complex Android applications.
In this case, create your own Dockerfile with `FROM redemonbr/android-sdk:<version>-cmdline` and extend it.
Examplifying: the `tools` variant above is an extension of this one. Check our GitLab repositories to see how the Dockerfile extends this one.

## Source of these images
-   Repository: <https://gitlab.com/rdnxk/android-sdk-docker>
-   Issues: <https://gitlab.com/rdnxk/android-sdk-docker/-/issues>
-   Dockerfiles: <https://gitlab.com/rdnxk/android-sdk-docker/-/tree/master/templates>

These images are updated on a weekly basis.

## Extending the image with environment variables
The images generated contains entrypoint scripts that may help you in extending the image. For the basic functionality, nothing needs to be done.
But if you need some extra customization, these might be helpful

### Locales Variables
_THIS IS NOT SUPPORTED IN ALPINE IMAGE VARIANTS_
The images already contain the locale `en_US.UTF-8` generated and set in the environment variables `LANG` and `LC_ALL` (also `LANGUAGE=en_US:en`).
If you need another `LANG` that is not already generated, you can set it like below:
-   **`ASDK_EXTRA_LANGS`:** generate new locales on entrypoint
-   **`LANG`:** change `LANG` shell variable
```bash
$ docker run --rm -it --name node-android-10 --env "ASDK_EXTRA_LANGS=en_GB.UTF-8,de_AT.UTF-8" --env "LANG=de_AT.UTF-8" redemonbr/android-sdk:node-14-api-29 /bin/bash
Generating locales (this might take a while)...
  de_AT.UTF-8... done
  en_GB.UTF-8... done
  en_US.UTF-8... done
Generation complete.
root@a410113043f0:/# _
```
Note that `en_US.UTF-8` still being generated by default.

Also, if you need to add more languages, `ASDK_EXTRA_LANGS` can receive multiple languages in CSV format, like this:
```bash
$ docker run --rm -it --name buster --env "ASDK_EXTRA_LANGS=en_GB.UTF-8,en_US.UTF-8,pt_BR,pt_BR.UTF-8" --env "LANG=en_GB.UTF-8,pt_BR" redemonbr/android-sdk:buster /bin/bash
Generating locales (this might take a while)...
  en_GB.UTF-8... done
  en_US.UTF-8... done
  pt_BR.ISO-8859-1... done
  pt_BR.UTF-8... done
Generation complete.
root@a410113043f0:/# _
```

If you don't want this behavior, just let the ASDK_EXTRA_LANGS environment variable unset.

### Using sdkmanager CLI
If you need to install more Android Packages, you can use `sdkmanager` CLI.
For example, if you need to install `Android Build Tools` on a specific version (`30.0.3` in this example):
```bash
## To automatically accept the license (if you already have read it and agreed to it)
## You can echo 'y' to the sdkmanager command
## This is useful in non-interactive environments
$ echo y | sdkmanager --install "build-tools;30.0.3"
```
```bash
## In interactive environments you don't need to echo 'y'
$ sdkmanager --install "build-tools;30.0.3"
```


## Contrinuous Integration example
### GitLab-CI
Using on GitLab-CI with an [Ionic V3](#https://ionicframework.com), [Cordova](https://cordova.apache.org) and Angular v5.
Consider all build dependencies (Angular, Ionic and Cordova) declared in `package.json`'s `devDependencies`, so `npm install` should install them without the need to explicitly install them globally.
Also, their specific commands can be prefixed with `npx`. [Read more about npx here](https://docs.npmjs.com/cli/v7/commands/npx)
```yml
build:android:
  stage: build
  image: redemonbr/android-sdk:node-14-api-30
  variables:
    # this will generate a new locale on entrypoint
    ASDK_EXTRA_LANGS: en_GB.UTF-8
  script:
    - npm install
    - npx ionic cordova build android --prod
  after_script:
    # copying generated build for saving it as an artifact
    - cp platforms/android/app/build/outputs/apk/debug/app-debug.apk my-app-debug.apk
  artifacts:
    paths:
      - my-app-debug.apk
    expire_in: 1 month
    public: false
```

## Note for Apache Cordova users on NodeJS image variants
Since `cordova-android>=10.0.0` Java/JDK 11 is supported. NodeJS variants including Java/JDK 8 are deprecated.
If you are still using `cordova-android<10.0.0`, please update to a newer version that supports Java/JDK 11.
Read the release post from Apache Cordova about this [here](https://cordova.apache.org/announcements/2021/07/20/cordova-android-10.0.0.html)

## Note for deprecated images
These images have no support from upstream anymore and will not be built anymore. Note that, if you are still using one of them, please update to newer releases.

`node-17` - Images based on NodeJS v17 will not be built anymore starting in July 2022 in order to favor newer and LTS-to-be NodeJS v18. Recommend moving to `node-18`
`node-17-alpine` - Images based on NodeJS v17 Alpine will not be built anymore in July 2022 in order to favor newer and LTS-to-be NodeJS v18. Recommend moving to `node-18-alpine`
`impish` - Images based on Ubuntu Impish Indri will not be built anymore in July 2022 in order to favor newer and LTS version Jammy Jellyfish. Recommend moving to `jammy`

## FAQ
-   More Ubuntu- or Debian-based variants planned?
    -   No. Only active LTS and currently maintained versions will be supported
-   Any CentOS or RedHat image variants planned?
    -   No. If you want glib and friends, checkout the Ubuntu- or Debian-based variants
-   More NodeJS images planned?
    -   No. Only NodeJS active LTS and currently maintained versions will be supported. The base OS on them are the latest Debian and Alpine versions (Bullseye and 3.15, respectivelly, at the time of this writing) will be supported
-   Any plans for using slim images from Debian?
    -   Nope. Heavy toolings are required for building Android apps, like JDK, Gradle and Android-SDKs. Using the slimmer images made no notable effect in the end size during my initial tests. I recommend checking out the Alpine variants for smaller pulls
